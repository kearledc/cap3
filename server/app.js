const express = require('express');
const mongoose = require('mongoose');
const app = express();

mongoose.connect('mongodb+srv://bigAdmin:admin123@bigandhuge-158vv.mongodb.net/WalkBuddy?retryWrites=true&w=majority',  {
	useNewUrlParser:true,
	useUnifiedTopology: true,
	useCreateIndex: true
})

mongoose.connection.once('open', () => {
	console.log('Lets Walk')
})

const server = require('./queries/queries');

server.applyMiddleware({
	app,
	path: "/capstone3"
})

const port = 3001;

app.listen(port, () =>{
	console.log(`I love you ${port}`);
})	