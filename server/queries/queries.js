const { ApolloServer, gql } = require('apollo-server-express');
const bcrypt = require('bcryptjs');

// Models
const User = require('../models/User');
const Role = require('../models/Role');
const Event = require('../models/Event');
const Category = require('../models/Category');
const Post = require('../models/Post');
// Components
const uuid = require('uuid/v1')
const fs = require('fs')

const typeDefs = gql`
	type UserType {
		id : ID!
		username : String!
		password: String!
		firstName : String!
		lastName : String!
		email : String!
		status: String
		role : String
		statusPost : [ String ]
		event: [ String ]
		payStatus : String
		roleId : RoleType
		image : String
		eventId : [EventType]
	}

	type RoleType {
		id : ID
		name: String
	}

	type EventType {
		id : ID
		name : String
		image : String
		category: [ String ]
		categoryId : [CategoryType]
		userId: [UserType]
	}

	type CategoryType {
		id : ID
		name : String!
		eventName : String!
		eventId: [EventType]
	}

	type PostType {
		id : ID
		content : String!
		creator : UserType
		createdAt: String!
		updatedAt: String!
	}

	type Query{
		#Retrieve
		hello : String
		getUsers : [UserType]
		getRoles : [RoleType]
		getEvents : [EventType]
		getCategories : [CategoryType]
		getPosts : [PostType]
	}

	input UserInput {
		username: String
		password: String
		firstName: String
		lastName: String
		status: String
		payStatus: String
		role: String
		event: [ String ]
		statusPost: [ String ]
	}

	input PostInput {
		content: String
	}

	type Mutation{
		#Create
		createUser(
			firstName: String!
			username: String!
			password: String!
			lastName: String!
			status: String
			payStatus: String
			email: String!
			stausPost : [ String ]
			role: String
			event: [ String ] 
		) : UserType

		createRole(
			name: String!
		) : RoleType

		createEvent(
			name : String!
			category : [ String ]
			image : String
		) : EventType

		createCategory(
			name: String!
			eventName: String
		) : CategoryType

		createPost(
			content: String!
		) : PostType

		#Update
		updatePost(
			id: ID!
			postInput : PostInput
		) : PostType
		updateUser(
			id: ID!
			userInput: UserInput
		) : UserType
		updateEvent(
			id: ID!
			name: String
			category: String
		) : EventType
		updateCategory(
			id: ID!
			name: String
			eventName: String
		) : CategoryType
		updateRole(
			name: String
		) : RoleType
		#Delete
		deleteUser(
			id: String
		) : Boolean

		deleteEvents(
			id: String
		) : Boolean

		deletePost(
			id: String
		) : Boolean

		#Login
		logInUser(
			username: String
			password: String
		) : UserType
	}
`;

const resolvers = {
	Query: {
		getUsers : () => User.find({}),
		getRoles : () => Role.find({}),
		getEvents : () => Event.find({}),
		getCategories : () => Category.find({}),
		getPosts : () => Post.find({})
	},

	Mutation: {
		// Create
		createUser : (_, {username,password,firstName,lastName,email}) => {
			// imageString = args.image
			// let imageBase = imageString.split(";base64,").pop()
			// let image = "images/" +uuid()+ ".jpg"
			// fs.writeFile(imageLocation, imageBase, {encoding: "base64"}, err=>console.log(err))
			let newUser = User({
				username,
				password: bcrypt.hashSync(password, 10),
				firstName,
				lastName,
				payStatus: "Not Paid ",
				status: "Run with Someone",
				email,
				statusPost : [],
				event: [],
				role: "Guest",
			})

			return newUser.save();
		},
		createRole : (_, {name}) => {
			let newRole = Role({
				name
			})

			return newRole.save();
		},
		createEvent : (_, {name, category}) => {
			let newEvent = Event({
				name,
				category,
				image: "image"
			})

			return newEvent.save();
		},
		createCategory : (_, {name, eventName}) => {
			let newCategory = Category({
				eventName,
				name
			})
			return newCategory.save();
		},
		
		createPost : (_, {content}) => {
			let newPost = Post({
				content
			})
			return newPost.save()
		},
		//Update
		updatePost : (_, {id , postInput}) => {
			return Post.findByIdAndUpdate(id, postInput, {new: true})
			.then(res => {
				return res;
			}).catch(err => console.log(err))
		},

		updateUser : (_, {id, userInput}) => {
			return User.findByIdAndUpdate(id, userInput, {new: true})
			.then(res => {
				return res;
			}).catch(err => console.log(err))

		},
		deletePost : (_, {id}) =>{
			return Post.findByIdAndDelete(id, (err, post) => {
				if(!post){
					console.log('No Post Found')
					return false
				}
				else{
					console.log('Post Deleted')
					return true
				}
			})
		},

		deleteUser : (_, {id}) => {
			return User.findByIdAndDelete(id, (err, user) => {
				if(!user){
					console.log('No User Found')
					return false
				}
				else{
					console.log('User Deleted')
					return true
				}
			})
		},

		deleteEvents : (_, {id}) => {
			return Event.findByIdAndDelete(id, (err, event) => {
				if(!event){
					return false
				}
				else{
					return true
				}
			})
		},
		logInUser : (_, args) => {
			// console.log(args)
			return User.findOne({ username: args.username }).then( user => {
				// console.log(user)
				if(user === null){
					console.log('User not found')
					return null
				}

				if(user.password === args.password){
					console.log('Welcome')
				}

				let hashedPassword = bcrypt.compareSync(args.password, user.password)
				console.log(hashedPassword)
				if(!hashedPassword){
					console.log('Wrong')
					return null
				}
				else{
					console.log('Welcome to the Server')
					return user
				}
			})
		}

	},
	UserType : {
		roleId : (_,{name}) => {
			return Role.findOne({name: _.role})
		},
		eventId : (_,{name}) => {
			return Event.find({name: _.event})
		}
	},

	EventType : {
		userId : (_,{event}) => {
			return User.find({event: _.name})
		},
		categoryId : (_,{name}) => {
			return Category.find({category: _.name})
		}
	},

	CategoryType : {
		eventId : (_,{name}) => {
			return Event.find({name: _.eventName})
		}
	}

}


const server = new ApolloServer({
	typeDefs,
	resolvers
})

module.exports = server;