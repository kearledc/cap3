const mongoose = require('mongoose');
const Schema = mongoose.Schema;

categorySchema = new Schema({
	name: {
		type: String,
		required: [true, 'Enter a Category']
	},

	eventName: {
		type: String
	}
})

module.exports = mongoose.model('Category', categorySchema)