const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
		username : {
			type: String,
			required: [true, 'Enter a username']
		},
		password : {
			type: String,
			required: [true, 'Enter a Password']
		},
		firstName : {
			type: String,
			required: [true, 'Enter a First Name']
		},
		lastName : {
			type: String,
			required: [true, 'Enter a Last Name']
		},
		email : {
			type: String,
			require: [true, 'Enter a valid Email']
		},

		statusPost : {
			type: [ String ]
		},

		status : {
			type: String
		},
		role : {
			type: String
		},
		image : {
			type: String
		},
		event : {
			type: [ String ]
		},
		payStatus : {
			type: String
		}
	},
	{
		timestamps: true
	}

)


module.exports = mongoose.model('User', userSchema);