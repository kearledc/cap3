import React from 'react';
import { Link } from 'react-router-dom';
// CSS
import './AdminView.css'
// Component
import AdminCreate from './AdminCreate'
import AdminNavbar from './AdminNavbar'
const AdminView = () => {

	return(
		<div className="adminViewContainer">
			<AdminCreate />
		</div>
	)
}

export default AdminView