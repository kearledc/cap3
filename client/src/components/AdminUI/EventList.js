import React from 'react';
// CSS
import './EventList.css'
// GraphQL
import {graphql} from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// Queries & Mutations
import { deleteUserMutation, updateUserMutation } from '../../queries/mutations'
import { getUsersQuery } from '../../queries/queries';
// Swal
import Swal from 'sweetalert2'
// Components
import AdminNavbar from './AdminNavbar'

const EventList = props => {
	console.log(props)
	const userData = props.getUsersQuery.getUsers? props.getUsersQuery.getUsers : [];
	// console.log(userData)
	
	const onFormSubmit = e => {
		e.preventDefault()
		let id = e.target.id.value
		let payStatus = e.target.payStatus.value
		console.log(id)
		let updatedUser = {
			payStatus
		}
		props.updateUserMutation({
				variables: {id, updatedUser}
			})
			.then(res => {
				console.log(res);
			});
	}

	const deleteUser = e => {
		console.log(e.target.id)
		let id = e.target.id

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteUserMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getUsersQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"User has been deleted.",
					"success"
				);
			}
		});

	}
	return(
		// Start of EventList Container
		<div className="eventListsContainer">
			<AdminNavbar />
			<table className="table mt-4 table-striped">
				<thead className="thead-dark">
					<tr className="eventListTable">
						<th>Event Name</th>
						<th>Username</th>
						<th>Name</th>
						<th></th>
						<th className="thAction" >Status</th>
						<th className="thAction">Action</th>
					</tr>
				</thead>
				<tbody>
					{userData.map(user => {
						return(
					<tr key={user.id}>
						<td>{user.event[0]}</td>
						<td>{user.username}</td>
						<td>{`${user.firstName} ${user.lastName}`}</td>
						<td>{user.payStatus}</td>											
						<td>
							<form onSubmit={onFormSubmit} className="tdForm">
								<input type="hidden" value={user.id} name="id"/>	
								<select name="payStatus" className="custom-select">
									<option>Not Paid</option>
									<option>Paid</option>
								</select>														
								<button className="btn btn-success btn-block ml-5" type="submit" id={user.id}>Update</button>							
							</form>
						</td>					
						<td>
							<button className="btn btn-danger btn-block" id={user.id} onClick={deleteUser}>Delete</button>						
						</td>
					</tr>						
					)})}
				</tbody>
			</table>
		</div>
		// End of EventList Container
	)
}


export default compose(
	graphql(getUsersQuery, {name: "getUsersQuery"}),
	graphql(deleteUserMutation, {name: "deleteUserMutation"}),
	graphql(updateUserMutation, {name: "updateUserMutation"})
)(EventList);

