import React from 'react';
import { Link } from 'react-router-dom'
// CSS
import './AdminNavbar.css'

const AdminNavbar = () => {
	return(
		<div className="adminNavbar">
			<nav className="navbar navbar-expand-lg navbar-light bg-light adminNavList">
			  <a className="navbar-brand" href="#">Walk Buddy</a>
			  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			    <span className="navbar-toggler-icon"></span>
			  </button>
			  <div className="collapse navbar-collapse" id="navbarNav">
			    <ul className="navbar-nav">
				    <Link to="/admincreate" className="nav-item userNavLinks adminLinks">
				        Create an Event
				  	</Link>
				    <Link to="/adminevents" className="nav-item userNavLinks adminLinks">
						Show Events
					</Link>
			    </ul>
			    <ul className="navbar-nav ml-auto">
			 	
			    </ul>
			  </div>
			</nav>
		</div>
	)
}

export default AdminNavbar