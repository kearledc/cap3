import React from 'react';
// GraphQL
import { graphql } from 'react-apollo';
// Lodash
import { flowRight } from 'lodash';
// Queries & Mutations
import { getEventsQuery } from '../../queries/queries'
// Components
import AdminNavbar from './AdminNavbar';

const AdminEvents = props => {
	let eventData = props.getEventsQuery.getEvents? props.getEventsQuery.getEvents : []
	console.log(eventData)
	let eventList = eventData.map(events =>{ 
		return(
		<tr key={events.id}>
			<td>{events.name}</td>
		</tr>
		)
	})
	return(
		<div className="adminEventsContainer">
			<AdminNavbar />
			<table className="table table-striped">
				<thead className="thead-dark">
					<tr>
						<th>Event Name</th>
					</tr>
				</thead>
				<tbody>
					{eventList}
				</tbody>
			</table>
		</div>
	)
}

export default graphql(getEventsQuery, {name:"getEventsQuery"})(AdminEvents);