import React, {useState} from 'react'
import { Redirect } from 'react-router-dom'
// Mutation & Queries
import { createEventMutation } from '../../queries/mutations'
import { getEventsQuery } from '../../queries/queries'
// GraphQL
import {graphql} from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// Components
import AdminNavbar from './AdminNavbar'
// CSS
import './AdminCreate.css'
import Swal from 'sweetalert2'

const AdminCreate = props => {
	console.log(props)
	const [register, setRegister] = useState(false);
	const onFormSubmit = e => {
		e.preventDefault()
		let name = e.target.name.value.trim()
		
		if(name.length < 3 && name.length === 0){
			Swal.fire({
				title: 'Event Name Invalid',
				text: 'Enter Valid Name',
				type: 'Error'
			})
			return;
		}

		let newEvent = {
			name
		};
		props.createEventMutation({
			variables: newEvent,
			refetchqueries: [{
				queries: getEventsQuery
			}]
		}).then(res => {	
		if(!res.data.createEvent.id){
				return
			}
			else {
				setRegister(true)
				console.log(register)
				Swal.fire({
					title: 'Successfully Created Event!',
					text: 'Welcome, Tara Walk Tayo',
					type: 'success'
				});
			}
		})

		return <Redirect to="/adminevents" />

	}

	return(
		<div className="adminCreateContainer">
			<AdminNavbar />
			<div className="formContainer">
				<div className="headerContainer">
					<h1 className="headerG">Create your Event</h1>
				</div>
				
				<form onSubmit={onFormSubmit}>
					<label htmlFor="name" className="adminCreate">Event Name</label>
					<input type="text" name="name" className="form-control adminInputEventCreate"/>
					<button type="submit" className="btn btn-block btn-outline-success">Create Event</button>
				</form>
			</div>
		</div>
	)
}

export default compose(
	graphql(createEventMutation, {name:"createEventMutation"}),
	graphql(getEventsQuery, {name:"getEventsQuery"})
	)(AdminCreate);