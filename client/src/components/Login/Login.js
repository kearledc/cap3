import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom'
// CSS
import './Login.css'
// Queries & Mutations
import { getUsersQuery } from '../../queries/queries'
import { logInUserMutation } from '../../queries/mutations'
// GraphQL
import { graphql } from 'react-apollo'
// Lodash
import { flowRight as compose } from 'lodash'
// Sweetalert2
import Swal from 'sweetalert2'

 
const Login = props => {	
	console.log(props)
	let login = props.logInUserMutation? props.logInUserMutation: [];
	const [logInSuccess, setLogin] = useState(false);
	const onLoginSubmit = e => {
		e.preventDefault()
		let username = e.target.username.value.trim()
		let password = e.target.password.value.trim()
		login({
			variables: {
				username,
				password
			}
		})
		.then(res => {
			let data = res.data.logInUser
			// console.log(data)
			if(data === null){ 
				Swal.fire({
					title: 'User Not Found',
					text: 'Please Register',
					type: 'error'
				})
			}
			else{	
				localStorage.setItem('username' , data.username)
				localStorage.setItem('password' , data.password)
				localStorage.setItem('firstName' , data.firstName)
				localStorage.setItem('lastName' , data.lastName)
				localStorage.setItem('email' , data.email)
				localStorage.setItem('id' , data.id)
				setLogin(true)
				props.updateSession()
				Swal.fire({
					title: 'Welcome',
					text: 'Tara Walk Us!',
					type: 'success'
				})
			}
		})
	}
	if(!logInSuccess){
		console.log('Not Good')
	}
	else{
		console.log('Great!')
		return <Redirect to="/userprofile" />
	}
	let userData = props.getUsersQuery.getUsers? props.getUsersQuery.getUsers : []	
	let userRole = userData.map(user => {
		if(user.role === "Guest") {
			return <Redirect to="/userlanding" />
		}
		else if(user.role === "Admin"){
			return <Redirect to="/adminevents" />
		}
	})


	return(
		// Start of loginContainer
		<div className="loginContainer">
			<div className="formContainer">
				<div className="introTexts">
					<h1 className="headerText">Walk Buddy</h1>
					<p className="headerSubText">Tara, Walk</p>
				</div>
				<form onSubmit={onLoginSubmit}>
					<div className="form-group inputContainer">
						<label htmlFor="username" className="inputLogin">Username:</label>
						<input type="text" className="form-control inputLog" name="username"/>
					</div>
					<div className="form-group inputContainer">
						<label htmlFor="password" className="inputLogin">Password:</label>
						<input type="password" className="form-control inputLog" name="password"/>
					</div>

					<button className="btn btn-success btn-outline btn-block btn-outline signLogin">Login</button>
				</form>
				<div className="signUpContainer mt-5">
					<Link to={'/register'}>
						<button className="btn btn-outline btn-primary signLogin"> Sign-Up </button>
					</Link>
				</div>
			</div>	
		</div>
		// End of Container
	)
}

export default compose(
	graphql(logInUserMutation, {name:"logInUserMutation"}),
	graphql(getUsersQuery, {name:"getUsersQuery"})
	)(Login);