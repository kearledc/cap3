import React from 'react';
// GraphQL
import {graphql} from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// CSS
import './EventCards.css'
// Queries
import { getEventsQuery } from '../../queries/queries';



const EventCards = props => {
	
	
	const eventData = props.getEventsQuery.getEvents? props.getEventsQuery.getEvents : []
	let eventLists = eventData.map(event => {
		return(
			<div key={event.id} className="eventCard animated flipInX">
				<h1>{event.name}</h1>
				<button className="btn btn-outline-danger joinButton"> Join Event </button>
			<img src="" alt=""/> {/* dito ko ilalagay yung image dynamic sana with {event.image} kaso hindi ko pa mapasok sa gql */}
			</div>	
		)
	})

	const onClickSubmit = e => {
		e.preventDefault()
		console.log('hello')
	}

	return(
		<div className="eventCardsCon">
			<form onSubmit={onClickSubmit}>	
				<div className="eventCardsList">			
					{eventLists}
				</div>
			</form>	
		</div>
	)
}

export default compose( 
	graphql(getEventsQuery, {name:"getEventsQuery"})
	) 
(EventCards);