import React, { useState } from 'react';
//CSS
import './UI.css';
// Queries & Mutations
import { getPostsQuery } from '../../queries/queries'
import { createPostMutation, deletePostMutation } from '../../queries/mutations'
// GraphQL
import {graphql} from 'react-apollo'
// Lodash
import {flowRight as compose} from 'lodash'
// Sweetalert
import Swal from 'sweetalert2'


const UserStatus = props => {
	let onStatusSubmit = e => {
		e.preventDefault()
		let content = e.target.content.value.trim()
		let newStatus = {
			content
		}
		props.createPostMutation({
			variables: newStatus,
			refetchQueries: [{
				query : getPostsQuery
				}]
		})
	}
	let postData = props.getPostsQuery.getPosts? props.getPostsQuery.getPosts : []
	let postList = postData.map(posts => {
	const deletePost = e => {
		let id = e.target.id
		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deletePostMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getPostsQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"Post has been deleted.",
					"success"
				);
			}
		});
	}
		return(
			<div key={posts.id} className="postLists">
				<li>{posts.content}</li>
				<button id={posts.id} className="btn-danger btn" onClick={deletePost}><i class="fas fa-trash-alt"></i></button>
			</div>	
		)
	})
	return(
		<div className="statusCon">
			<form onSubmit={onStatusSubmit}>
				<label htmlFor="status" className="statusLabel">Status:</label>
				<div className="input-group mb-3">
				  <input type="text" name="content" className="form-control inputStatus" placeholder="What's Up?" aria-label="Recipient's username" aria-describedby="button-addon2" />
				  <div className="input-group-append">
				    <button className="btn btn-outline-primary" type="submit" id="button-addon2">Submit</button>
				  </div>
				</div>	
			</form>
			<ul>
				{postList}
			</ul>			
		</div>
	)
}

export default compose(
	graphql(getPostsQuery, {name:"getPostsQuery"}),
	graphql(createPostMutation, {name:"createPostMutation"}),
	graphql(deletePostMutation, {name:"deletePostMutation"})
	)(UserStatus);