import React from 'react';
//GraphQL
import {graphql} from 'react-apollo'
//Lodash
import {flowRight as compose} from 'lodash'
//Queries
import {getEventsQuery} from '../../queries/queries'
//CSS
import './UI.css'

const UserEvents = props => {
	// console.log(props)
	let eventData = props.getEventsQuery.getEvents? props.getEventsQuery.getEvents : []
	// console.log(eventData)
	let eventTable = eventData.map(events => {
			return(
				<tr key={events.id}>
					<td className="tableEvents">{events.name}</td>
					<td className="tdButton text-right">
						<button className="btn btn-outline-success">Join This Event</button>
					</td>
				</tr>
			)
		})
	return(
		<div className="userEventsContainer">
			<table className="table table-striped">
				<thead className="thead-dark eventTableHead">
					<th colSpan="2">Events</th>
				</thead>
				<tbody>
					{eventTable}
				</tbody>
			</table>
		</div>
	)
}

export default graphql(getEventsQuery, {name:"getEventsQuery"})(UserEvents)