import React from 'react';
//Carousel
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel'
//CSS
import './UI.css'

const UserCarousel = () => {
	return(
		<div className="CarouselContainer">
			<Carousel showThumbs={false} infiniteLoop={true} showStatus={false} autoPlay={true}>
				<div>
					<img src="/images/air.jpg" alt="Air" className="imgCars"/>
				</div>
				<div>
					<img src="/images/fire.jpeg" alt="Fire" className="imgCars"/>
				</div>
				<div>
					<img src="/images/water.jpeg" alt="Water" className="imgCars"/>
				</div>
			</Carousel>
		</div>
	)
}

export default UserCarousel