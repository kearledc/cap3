import React from 'react';
// GraphQL
import { graphql } from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// Queries & Mutations
import { getUsersQuery } from '../../queries/queries';

// CSS
import './UI.css';	
const UserInfo = props => {
	let userData = props.getUsersQuery.getUsers? props.getUsersQuery.getUsers : []
	let userName = <h3 className="userGreeting">{`${localStorage.getItem('firstName')} ${localStorage.getItem('lastName')}`}</h3>
	return(
		<div className="userInfoContainer">
			{userName}
			<button className="btn btn-success letsRunButton animated infinite pulse">Let's Run</button>
		</div>
	)
}

export default compose(
	graphql(getUsersQuery, {name:"getUsersQuery"})
	)(UserInfo);
