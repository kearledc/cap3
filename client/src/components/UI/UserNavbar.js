import React, { useState } from 'react';
import { Link } from 'react-router-dom'
// CSS
import './UI.css'

const UserNavbar = props => {
	// console.log(localStorage.username)
	let username = localStorage.username
	let userGreeting = username?
	<li className="nav-item">
		{`Hello ${localStorage.firstName}!`}
	</li>
	:
	<li className="nav-item">
		{"Hello Guest"}
	</li>
	let userAvailable = username?
	<Link className="nav-item logOut" to="/logout">
		Logout
	</Link>
	:
	<Link className="nav-item" to="/">
		Login
	</Link>


	return(
		<div className="userNavbarContainer">
				<nav className="navbar navbar-expand-lg navbar-light userNavbarList">
				  <a className="navbar-brand logoHeader" href="#">Walk Buddy</a>
				  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				    <span className="navbar-toggler-icon"></span>
				  </button>
				  <div className="collapse navbar-collapse" id="navbarNav">
				    <ul className="navbar-nav">
				    <Link to={`/userprofile/${localStorage.getItem('id')}`} className="nav-item userNavLinks">
				        My Profile
				  	</Link>
				    <Link to="/userlanding" className="nav-item userNavLinks">
						Events
					</Link>
				    <Link className="nav-item userNavLinks" to="/userupdate">
				    	Edit Profile
				    </Link>
				    </ul>
				    <ul className="navbar-nav ml-auto">
				    {userGreeting}
				    {userAvailable}
				    </ul>
				  </div>
				</nav>
		</div>
	)
}

export default UserNavbar