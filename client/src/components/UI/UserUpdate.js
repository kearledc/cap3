import React from 'react';
// Components
import UserNavbar from './UserNavbar'
// GraphQL
import { graphql } from 'react-apollo'
// Lodash
import { flowRight as compose } from 'lodash'
// Queries & Mutations
import { getUsersQuery } from '../../queries/queries'
import { updateUserMutation } from '../../queries/mutations'

const UserUpdate = props => {
	let userData = props.getUsersQuery.getUsers? props.getUsersQuery.getUsers : []
	let idInput = userData.map(user => {
		return(
			<input type="hidden" name="id" value={user.id} key={user.id}/>
		)
	})	
	console.log(localStorage.getItem('id'))
	let onUpdateSubmit = e => {
		e.preventDefault()
		let username = e.target.username.value.trim();
		let firstName = e.target.firstName.value.trim();
		let lastName = e.target.lastName.value.trim();
		let status = e.target.status.value
		let id = localStorage.getItem('id')		
		console.log(id)
		let updatedUser = {
			username,
			firstName,
			status,
			lastName
		}
		console.log(updatedUser)
		props.updateUserMutation({
			variables: { id, updatedUser }
		}).then(res => console.log(res)).catch(err => console.log(err))
	}

	console.log(userData)

	return(
		<div className="updateContainer">
			<UserNavbar />
			<div>
				<h1 className="text-center mt-1"> Edit Your Profile </h1>
				<form className="updateForm" onSubmit={onUpdateSubmit}>
					<label htmlFor="firstName" className="updateUserLabel">First Name:</label>
					<input type="text" name="firstName" className="inputUserUpdate form-control"/>

					<label htmlFor="lastName" className="updateUserLabel">Last Name:</label>
					<input type="text" name="lastName" className="inputUserUpdate form-control"/>

					<label htmlFor="username" className="updateUserLabel">User Name:</label>
					<input type="text" name="username" className="inputUserUpdate form-control"/>				
					{idInput}
					
					<select name="status" id="status" className="updateUserLabel" className="custom-select selectUserUpdate">
						<option>Run With Someone</option>
						<option>Run Alone</option>
					</select>

					<button className="btn btn-outline-primary updateButton">Update</button>
				</form>
			</div>
		</div>
	)
}

export default compose(
	graphql(updateUserMutation, {name:"updateUserMutation"}),
	graphql(getUsersQuery, {name:"getUsersQuery"})
	)(UserUpdate);