import React from 'react';
//GraphQL
import {graphql} from 'react-apollo'
//Lodash
import {flowRight as compose} from 'lodash'
//Queries
import {getUsersQuery} from '../../queries/queries'
// Components
import UserNavbar from './UserNavbar';
import UserEvents from './UserEvents';
import UserCarousel from './UserCarousel';
//CSS
import './UI.css'

const UserLanding = props => {
	return(
		<div className="userLandingContainer">
			<UserNavbar />
			<UserCarousel />
			<UserEvents />
		</div>
	)
}

export default compose(
	graphql(getUsersQuery, {name:"getUsersQuery"})
	)
(UserLanding);