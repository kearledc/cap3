import React from 'react';
// CSS
import './UI.css';
// Components
import UserNavbar from './UserNavbar';
import UserStatus from './UserStatus';
import UserInfo from './UserInfo'


const UserProfile = () => 
(
		<div className="userProfileContainer">
			<UserNavbar />
			<div className="userContainer">
				<div className="row">
					<div className="col-12 col-md-12 col-lg-4">
						<UserInfo />
					</div>
					<div className="col-12 col-md-12 col-lg-8">
						<UserStatus />
					</div>
				</div>
			</div>
		</div>
)



export default UserProfile