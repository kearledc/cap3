import React from 'react';
// Queries
import { getUsersQuery } from '../queries/queries';
// GraphQL
import { graphql } from 'react-apollo';
const UserNames = props => {
	// console.log(props)
	let userData = props.getUsersQuery.getUsers? props.getUsersQuery.getUsers : []
	
	

	return(
		<div className="userNamesContainer">
			{userData.map(user => {
				return(
					<h2>{user.firstName}</h2>			
			)})}			
		</div>
	)
}

export default graphql(getUsersQuery, {name:"getUsersQuery"})(UserNames);