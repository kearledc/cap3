import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
// CSS
import './Register.css';
// Mutations & Queries
import { getUsersQuery } from '../../queries/queries'
import { createUserMutation } from '../../queries/mutations'
// GraphQL
import {graphql} from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// toBase64
import { toBase64, nodeServer } from '../../function.js'
// Sweetalert
import Swal from 'sweetalert2'

const Register = props => {

	const [register, setRegister] = useState(false);
	const onFormSubmit = e => {	
		e.preventDefault()
		let firstName = e.target.firstName.value.trim()
		let lastName = e.target.lastName.value.trim()
		let email = e.target.email.value.trim()
		let username = e.target.username.value.trim()
		let password = e.target.password.value.trim()
		
		if(firstName.length === 0 && firstName.length < 3){
			Swal.fire({
				title: 'First Name Invalid',
				text: 'Enter Valid Name',
				type: 'Error'
			})
			return;
		};
		if(lastName.length === 0 && lastName.length < 3){
			Swal.fire({
				title: 'Last Name Invalid',
				text: 'Enter Valid Name',
				type: 'Error'
			})
			return;
		};
		if(username.length < 8 && username === null){
			Swal.fire({
				title: 'Username Invalid',
				text: 'Enter Valid Name',
				type: 'Error'
			})
			return;
		};

		if(password.length < 8 && password === null){
			Swal.fire({
				title: 'Password Invalid',
				text: 'Enter Valid Name',
				type: 'Error'
			})
			return;
		};
			
		let newUser = {
			firstName,
			lastName,
			email,
			username,
			password,
		}
			
		props.createUserMutation({
			variables : newUser,
			refetchQueries: [{
				query : getUsersQuery
				}]
		})
		.then(res => {
			if(!res.data.createUser.id){
				return
			}
			else {
				setRegister(true)
				console.log(register)
				Swal.fire({
					title: 'Successfully Registered!',
					text: 'Welcome, Tara Walk',
					type: 'success'
				});
			}
		})
	}


	return(
		// Start of Register Container
		<div className="registerContainer">
			<div className="formContainer2">
				<div className="registerHeading">
					<h1>Register Here</h1>
				</div>
				<form onSubmit={onFormSubmit}>
					<div className="form-group inputRegister">
						<label htmlFor="firstName" className="registerLabel">First Name:</label>
						<input type="text" className="form-control" name="firstName"/>

						<label htmlFor="lastName" className="registerLabel">Last Name:</label>
						<input type="text" className="form-control" name="lastName"/>

						<label htmlFor="email" className="registerLabel">E-mail:</label>
						<input type="email" className="form-control" name="email"/>

						<label htmlFor="username" className="registerLabel">Username:</label>
						<input type="text" className="form-control" name="username"/>

						<label htmlFor="password" className="registerLabel">Password:</label>
						<input type="password" className="form-control" name="password"/>

						<button className="btn btn-block mt-4 btn-outline-success">Register</button>
					</div>
				</form>
			</div>
		</div>
		// End of Register Container
	)
}

export default compose(
	graphql(createUserMutation, {name: "createUserMutation"}),
	graphql(getUsersQuery, {name: "getUsersQuery"})
	)(Register);