import {gql} from 'apollo-boost';

// Create
const createUserMutation = gql`
	mutation(
		$firstName: String!
		$lastName: String!
		$email: String!
		$username: String!
		$password: String!		
	) {
		createUser(
			firstName: $firstName,
			lastName: $lastName,
			email: $email
			username: $username,
			password: $password,			
		) {
			id
			firstName
			lastName
			email
			username
			password			
		}
	}
`;
const createEventMutation = gql`
	mutation($name: String!) {
		createEvent(
			name: $name
		) {
			id
			name
		}
	}
`;
const createPostMutation = gql`
	mutation($content: String!){
		createPost(
			content: $content
		) {
			id
			content
		}
	}
`;
// Update
const updateUserMutation = gql`
	mutation(
		$id: ID!
		$updatedUser : UserInput
	) {
		updateUser( id: $id , userInput: $updatedUser ){
			id
			firstName
			lastName
			status
			payStatus
			event
			username
			statusPost
		}
	}
`;

// const updateUserMutation = gql`
// 	mutation(
// 		$id: ID!
// 		$firstName: String
// 		$lastName: String
// 		$username: String
// 		$password: String
// 		$status: String
// 		$event: [ String ]
// 		$payStatus : String
// 		$statusPost : [ String ]
// 	) {
// 		updateUser(
// 			id: $id
// 			firstName : $firstName
// 			lastName : $lastName
// 			username : $username
// 			password : $password
// 			status : $status
// 			event : $event
// 			payStatus : $payStatus
// 			statusPost : $statusPost
// 		) {
// 			id
// 			firstName
// 			lastName
// 			statusPost
// 			status
// 			payStatus
// 			password
// 			event
// 			username
// 		}
// 	}
// `;
// Delete
const deleteUserMutation = gql`
	mutation($id: String!){
		deleteUser(id: $id)
	}
`;

const deletePostMutation = gql`
	mutation($id: String!){
		deletePost(id: $id)
	}
`;
// Login
const logInUserMutation = gql`
	mutation(
		$username: String!
		$password: String!
	) {
		logInUser(
		username: $username
		password: $password
		) {
			username
			password
			firstName
			lastName
			email
			id
		}
	}
`;


export {
	createUserMutation,
	createEventMutation,
	createPostMutation, 
	deleteUserMutation,
	deletePostMutation,
	updateUserMutation,
	logInUserMutation
};