import {gql} from 'apollo-boost';

const getUsersQuery = gql`
	{
	  getUsers{
	    username
	    password
	    status
	    role
	    payStatus
	    id
	    firstName
	    lastName
	    event
	  }
	}
`;

const getEventsQuery = gql`
	{
		getEvents{
			id
			name
			category
			categoryId{
				name
			}
			userId{
				username
			}
		}
	}	
`;

const getCategoriesQuery = gql`
	{
		getCategories{
			id
			name
			  eventName
			  eventId{
			    name
			  }
		}
	}
`;

const getPostsQuery = gql`
	{
		getPosts{
			id
			content
		}
	}
`;

export { getUsersQuery, getEventsQuery, getCategoriesQuery, getPostsQuery };