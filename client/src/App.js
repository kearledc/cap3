import React, { useState } from 'react';
// CSS
import './App.css';
// Components

import Login from './components/Login/Login';
import Register from './components/Register/Register'
import EventList from './components/AdminUI/EventList'
import EventCards from './components/EventCards/EventCards'
import UserLanding from './components/UI/UserLanding'
import UserUpdate from './components/UI/UserUpdate' 
import UserProfile from './components/UI/UserProfile'
import UserStatus from './components/UI/UserStatus'
import AdminView from './components/AdminUI/AdminView'
import AdminCreate from './components/AdminUI/AdminCreate'
import AdminEvents from './components/AdminUI/AdminEvents'

// React Router
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
// Apollo
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "react-apollo";
// Sweetalert2
import Swal from 'sweetalert2'


const backend = new ApolloClient ( { uri: "http://localhost:3001/capstone3"} )

function App() {
  const [username, setUsername] = useState(localStorage.getItem('username'))
  const [password, setPassword] = useState(localStorage.getItem('password'))
  
  const updateSession = () => {
    setUsername(localStorage.getItem('username'))
    setPassword(localStorage.getItem('firstName'))
  }
  
  const Logout = () => {
        localStorage.clear();
        updateSession()
        return <Redirect to="/" />
  }

  const loggedUser = props => <Login {...props} updateSession={updateSession} />

  return (
    <ApolloProvider client={backend}>
       <BrowserRouter>
         <div>
           <Switch>
             <Route exact path="/" component={loggedUser} />
             <Route path="/register" component={Register} />
             <Route path="/eventlist" component={EventList} />
             <Route path="/userlanding" component={UserLanding} />
             <Route path="/eventcards" component={EventCards} />
             <Route path="/adminview" component={AdminView} />
             <Route path="/admincreate" component={AdminCreate} />
             <Route path="/userprofile" component={UserProfile} />
             <Route path="/userlanding" component={UserLanding} />
             <Route path="/userupdate/" component={UserUpdate} />
             <Route path="/adminevents" component={AdminEvents} />
             <Route path="/logout" component={Logout} />            
             <Route path="/userstatus" component={UserStatus} />            
           </Switch>
         </div>
       </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
